/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */ 

import React, {useEffect, useState, useCallback} from 'react';
import database from '@react-native-firebase/database';
import {
  SafeAreaView,
  StyleSheet,
  Linking,
  View,
  Text,
  StatusBar,
  Image,
  ImageBackground,
  Platform,
} from 'react-native';

import {
  Header,
  Button,
  Avatar,
  Icon
} from 'react-native-elements';

import RNMusicStreamer from 'react-native-music-streamer';



const App = () => {
  const [metadata, setMetadata] = useState({})
  const url = "https://ponyvillefm.com"
  useEffect(() => {
    const onValueChange = database()
      .ref(`/rest/fillydelphia-radio/now-playing`)
      .on('value', snapshot => {
        setMetadata(snapshot.val())
      });
      return () =>
      database()
        .ref(`/rest/fillydelphia-radio/now-playing`)
        .off('value', onValueChange);
  }, [metadata]);
  handleHomepageClick = useCallback(async () => {
    const supported = Linking.canOpenURL(url)
      if (supported) {
        await Linking.openURL(url);
      } else {
        Alert.alert(`Don't know how to open this URL: ${url}`);
      }
    }, [url]);
  RNMusicStreamer.prepare(
    'https://fillyradio.com/stream-320k', // stream url
    {
      title: metadata.title, // "title" for notification (optional)
      album: metadata.album, // "album" for notification (optional)
      artist: metadata.artist, // "artist" for notification (optional)
      metadataFromStream: false, // If true, title will be set from stream metadata. Updates every 10s. 'title' property will be used as placeholder. (optional)
      artwork: metadata.title ? metadata.covers[384] : 'https://ponyvillefm.com/images/music/default.png', // artwork for notification. Either a url as string, or a react-native format {uri: ...} object for local image. (optional)
    }
  )
  return (
    <>
      <ImageBackground
        style={{flex: 1}}
        source={{uri: metadata.title ? metadata.covers[384] : 'https://ponyvillefm.com/images/music/default.png'}}
        blurRadius={4}>
        <View style={{flex: 1, backgroundColor: 'rgba(77,15,128,0.40)'}}>
          <Header
            containerStyle={styles.headerContainer} 
            statusBarProps={{ barStyle: 'light-content', backgroundColor: '#4D0F80' }}
            leftComponent={{ icon: 'menu', color: '#fff' }}
            centerComponent={<Image source={require('./assets/img/pvfm.png')} style={{height: 30, width: 200}} />}
            rightComponent={{ icon: 'globe-americas', type: 'font-awesome-5', color: '#fff', onPress: handleHomepageClick }}
            />
          <View style={{flex: 3, justifyContent: 'center', alignItems: 'center'}}>
            {metadata.title ?
            <>
              <Image source={{uri: metadata.covers[384]}} style={{height: 350, width: 350}}></Image>
            </> :
            <>
              <Image source={{uri: 'https://ponyvillefm.com/images/music/default.png'}} style={{width: 350, height: 350}}></Image>
            </>
            }
          </View>
          <View style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
          {metadata.title &&
            <>
              <Text style={styles.metaTitle}>{metadata.title}</Text>
              <Text style={styles.metaText}>by {metadata.artist}</Text>
              {
                metadata.album &&
              <Text style={styles.metaText}>from {metadata.album}</Text>
              }
            </>
          }
          </View>
          <View style={{flex: 0}}>
            


          </View>
          <View style={{flex: 1, flexDirection: "row", justifyContent: "center", alignItems: 'center'}}>
            <Button icon={<Icon name='play-arrow' color='white' type='fontawesome' size={50} /> } buttonStyle={{borderRadius: 35, height: 70, width: 70, backgroundColor: 'rgb(77,15,128)'}}></Button>
          </View>
        </View>
      </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    display: 'flex',
    flexDirection: 'row',
    height: Platform.select({
        android: 80,
        default: 44,
      }),
    alignItems: 'flex-end',
    backgroundColor: 'rgba(77,15,128,0.50)'
  },
  metaText: {
    textAlign: 'center',
    color: 'white'
  },
  metaTitle: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20
  }
});

export default App;
